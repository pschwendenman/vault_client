'''
vault client

helper for interacting with vault.
'''
from __future__ import print_function
import argparse
import json
import os
import uuid
import hvac

def initialize_vault(vault_address):
    '''
    Setup a new vault to match expectations.
    '''
    client = hvac.Client(url=vault_address)

    shares = 5
    threshold = 3

    result = client.initialize(shares, threshold)

    root_token = result['root_token']
    keys = result['keys']

    print(root_token)
    print(keys)

    client.unseal_multi(keys)

    client.enable_auth_backend('app-id')

def upload_secrets(vault_address, app_id, user_id, filename):
    '''
    Upload the provided secrets file vault
    '''
    client = hvac.Client(url=vault_address)
    client.auth_app_id(app_id, user_id)

    with open(filename) as file_object:
        secret = ''.join(file_object.readlines())

    client.write('secret/pipeline/{}'.format(app_id), data=secret)

    print(filename)

def download_secrets(vault_address, app_id, user_id):
    '''
    Read secrets from vault
    '''
    client = hvac.Client(url=vault_address)
    client.auth_app_id(app_id, user_id)

    print('secret/pipeline/{}'.format(app_id))

    response = client.read('secret/pipeline/{}'.format(app_id))

    print(json.dumps(response['data'], sort_keys=True,
                     indent=4, separators=(',', ': ')))

def create_credentials(vault_address, vault_token, team_name):
    '''
    Create APP_ID and USER_ID for a team and add to vault
    '''
    print(team_name)


    client = hvac.Client(url=vault_address, token=vault_token)
    app_id = str(uuid.uuid1())
    user_id = str(uuid.uuid4())

    policy = """
    path "sys" {
      policy = "deny"
    }

    path "secret/pipeline/%s" {
      policy = "write"
      capabilities = ["create", "read", "update", "delete", "list"]
    }
    """ % (app_id)

    print(policy)

    client.set_policy(team_name, policy)

    client.write('auth/app-id/map/app-id/{app_id}'.format(app_id=app_id),
                 value=team_name,
                 display_name=team_name)
    client.write('auth/app-id/map/user-id/{user_id}'.format(user_id=user_id),
                 value=app_id)

    print('Policies:', client.list_policies())

    print("Team: {team_name}".format(team_name=team_name))
    print("export VAULT_APP_ID={app_id}".format(app_id=app_id))
    print("export VAULT_USER_ID={user_id}".format(user_id=user_id))


def main(args_param=None):
    '''
    Parse Args and Start Sub task
    '''
    parser = argparse.ArgumentParser(
        description='tool for interacting with vault')
    subparsers = parser.add_subparsers(dest='subparser_name')
    parser_init = subparsers.add_parser('init')
    parser_init.set_defaults(func=initialize_vault)

    parser_create_user = subparsers.add_parser('create')
    parser_create_user.set_defaults(func=create_credentials)
    parser_create_user.add_argument('-t', '--teamname', required=True)

    parser_upload = subparsers.add_parser('upload')
    parser_upload.set_defaults(func=upload_secrets)
    parser_upload.add_argument('filename')

    parser_upload = subparsers.add_parser('download')
    parser_upload.set_defaults(func=download_secrets)

    args = parser.parse_args(args_param)

    # args.func(**vars(args))
    if args.subparser_name == 'upload':
        if 'VAULT_ADDR' not in os.environ:
            parser.error("Please set VAULT_ADDR environment variable")
        if 'VAULT_APP_ID' not in os.environ:
            parser.error("Please set VAULT_APP_ID environment variable")
        if 'VAULT_USER_ID' not in os.environ:
            parser.error("Please set VAULT_USER_ID environment variable")
        upload_secrets(os.environ['VAULT_ADDR'],
                       os.environ['VAULT_APP_ID'],
                       os.environ['VAULT_USER_ID'],
                       args.filename)
    elif args.subparser_name == 'download':
        if 'VAULT_ADDR' not in os.environ:
            parser.error("Please set VAULT_ADDR environment variable")
        if 'VAULT_APP_ID' not in os.environ:
            parser.error("Please set VAULT_APP_ID environment variable")
        if 'VAULT_USER_ID' not in os.environ:
            parser.error("Please set VAULT_USER_ID environment variable")
        download_secrets(os.environ['VAULT_ADDR'],
                         os.environ['VAULT_APP_ID'],
                         os.environ['VAULT_USER_ID'])
    elif args.subparser_name == 'init':
        if 'VAULT_ADDR' not in os.environ:
            parser.error("Please set VAULT_ADDR environment variable")
        initialize_vault(os.environ['VAULT_ADDR'])
    elif args.subparser_name == 'create':
        if 'VAULT_ADDR' not in os.environ:
            parser.error("Please set VAULT_ADDR environment variable")
        if 'VAULT_TOKEN' not in os.environ:
            parser.error("Please set VAULT_TOKEN environment variable")
        create_credentials(os.environ['VAULT_ADDR'],
                           os.environ['VAULT_TOKEN'],
                           args.teamname)

if __name__ == "__main__":
    main()
